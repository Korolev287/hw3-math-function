// Функции нужны для того что бы не повторять один и тот же код в разных частях программы, а так же для оптимизации программы.
// Аргументы функций нужны для вызова функций с разными аргументами, что бы переиспользовать функцию, а не создавать одноразовые функции.

const number1 = +prompt("Введите первое число");
const number2 = +prompt("Введите второе число");
const mathOperation = prompt("Введите операцию которую хотите совершить", "- + / *");

function mathCalculator(a ,b, operation) {
    if(operation === "/") {
        return a / b;
    } else if (operation === "*"){
    return a * b;
    }else if (operation === "-"){
        return a - b;
    }else if (operation === "+"){
        return a + b;
    }else {
        alert("Что-то пошло не так!")
    }
}

console.log(mathCalculator(number1, number2, mathOperation));